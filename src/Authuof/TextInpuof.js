import React from 'react'
import { StyleSheet, View, Text, TextInput } from 'react-native'

import Icon from 'react-native-vector-icons/FontAwesome'

export default props => {
    return (
        <View>
            <View style={styles.container}>
                {props.icon && <Icon name={props.icon} size={20} style={styles.icon} />}
                <TextInput {...props} style={styles.input} />
            </View>
            <Text style={styles.errors}>{props.err}</Text>
        </View>
    )
}

var styles = StyleSheet.create({
    container: {
        width: '100%',
        height: 40,
        // backgroundColor: '#EFEFEE',
        backgroundColor: '#EFEFDB',
        borderRadius: 10,
        flexDirection: 'row',
        alignItems: 'center',
    },
    icon: {
        color: '#3D3D3D',
        marginLeft: 10
    },
    input: {
        marginLeft: 10,
        width: '100%',
        height: 40
    },
    errors: {
        color: '#FA3D3D',
        fontSize: 12,
    }
})