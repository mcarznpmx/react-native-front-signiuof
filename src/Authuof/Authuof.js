import React, { Component } from 'react'
import { StyleSheet, View, Text, ImageBackground, TouchableOpacity, Platform } from 'react-native'

import { ClientError, ClientAxios } from 'mcarz-front-utils'

import TextInpuof from './TextInpuof'

export default class Authuof extends Component {
    state = {
        username: '',
        password: '',
        errors: {}
    }

    signin = () => {
        if (this.validateSignin()) {
            if (!this.props.endpoint || !this.props.signin){ return }
            /* processo event before call ajax request */
            this.props.onBefore && this.props.onBefore()
            
            ClientAxios(this.props.endpoint, {
                PlatformName: Platform.OS,
                PlatformVersion: Platform.Version
            }).post(this.props.signin, {
                username: this.state.username,
                password: this.state.password,
            })
            .then(res => this.props.onNext && this.props.onNext(false, res.data))
            .catch(err => {
                ClientError(err, (message, validations) => {
                    if (!validations) {
                        validations = {}
                    }
                    validations.message = message
                    this.setState({ errors: validations })
                    this.props.onNext && this.props.onNext(message, null)
                })
            })
            /* processo event after call ajax request */
            .finally(() => this.props.onAfter && this.props.onAfter())
        }
    }

    validateSignin = () => {
        let errors = {}
        
        if (!this.state.username || this.state.username.trim() == '') {
            errors.username = 'Usuário não pode ser vázio'
        }

        if (!this.state.password || this.state.password.trim() == '') {
            errors.password = 'Senha não pode ser vázia'
        }
        
        this.setState({ errors })
        
        return ((errors.username || errors.password) ? false : true)
    }
    render() {
        return (
            <ImageBackground source={this.props.imagePath} style={styles.background}>
                <View style={styles.title}>
                    <Text style={styles.text}>{this.props.title || 'Login'}</Text>
                </View>
                <View style={styles.container}>

                    <Text style={styles.alert}>{this.state.errors.message}</Text>

                    <TextInpuof
                        icon='user'
                        err={this.state.errors.username}
                        placeholder={this.props.usrLabel || 'Usuário...'}
                        value={this.state.username}
                        onChangeText={username => this.setState({ username })} />

                    <TextInpuof
                        icon='lock'
                        err={this.state.errors.password}
                        placeholder={this.props.passLabel || 'Senha...'}
                        value={this.state.password}
                        secureTextEntry={true}
                        onChangeText={password => this.setState({ password })} />

                    <TouchableOpacity onPress={this.signin}>
                        <View style={styles.button}>
                            <Text style={styles.buttonText}>{this.props.accessText || 'Acessar'}</Text>
                        </View>
                    </TouchableOpacity>
                </View>
                <View style={{ flex: 1 }}></View>
            </ImageBackground>
        )
    }
}

var styles = StyleSheet.create({
    background: {
        flex: 1,
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center',
    },
    container: {
        width: '90%',
        padding: 12,
        shadowColor: "#222",
        shadowOffset: { width: 0, height: 10 },
        shadowOpacity: 0.5,
        shadowRadius: 3.84,
        elevation: 6,
        backgroundColor: 'rgba(255,255,255, 0.9)'
    },
    button: {
        padding: (Platform.OS == 'ios' ? 12 : 4),
        marginTop: 4,
        alignItems: 'center',
        backgroundColor: '#3E7FAD',
    },
    buttonText: {
        color: '#FDFDFD',
        fontSize: 18
    },
    text: {
        fontSize: 32,
        color: '#EDEDEF'
    },
    title: {
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'center'
    },
    errors: {
        color: '#F93D3D',
        fontSize: 16,
    },
    alert: {
        color: '#F93D3D',
        textAlign: 'center',
        fontSize: 16
    }
})